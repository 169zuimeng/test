import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface IParking {

    //打印结果
    public void print();

    //检查车位，为空返回false，成功停入返回true
    public boolean addCar(int carType);

    //解析参数
    public static IParams parse() throws Exception{
        int init[] = new int[3];
        Scanner in = new Scanner(System.in);
        StringBuffer test=new StringBuffer();
        String o ="";
        Boolean flag=true;
        int i=0;
        int j=0;
        ArrayList<Integer> cartype = new ArrayList<>();
        while(!(o=in.next()).equals("exit")){

            if (o.contains("[[")) {
                flag=false;
            }

            if (flag){
                if (i==0){
                    if (o.equals("[\"ParkingSystem\",")){
                        i++;
                        continue;
                    }
                }else if(o.equals("\"addCar\",")||o.equals("\"addCar\"]")){
                    j++;
                    continue;
                }
                throw new Exception("格式错误");
            }else{
                Pattern p = Pattern.compile("[^0-9]");
                Matcher m = p.matcher(o);
                String num = m.replaceAll("").trim();
                if (num==""){
                    throw new Exception("格式错误");
                } else {
                    if (i <= 4) {
                        init[i] = Integer.valueOf(num);
                        i++;
                    } else {
                        i++;
                        cartype.add(Integer.valueOf(num));
                    }
                }
                if ((i-4)!=j){
                    throw new Exception("格式错误");
                }
            }
        }
        in.close();

        return new IParams() {
            @Override
            public int getBig() {
                return init[0];
            }

            @Override
            public int gerMedium() {
                return init[1];
            }

            @Override
            public int getSmall() {
                return init[2];
            }

            @Override
            public ArrayList<Integer> getPlanParking() {
                return cartype;
            }
        };
    }
}
